﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUtama
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.Button1 = New System.Windows.Forms.Button
        Me.tvw = New System.Windows.Forms.TreeView
        Me.CNetHelpProvider = New System.Windows.Forms.HelpProvider
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.SuspendLayout()
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.CNetHelpProvider.SetHelpKeyword(Me.SplitContainer1, "cdDVDmanager_Form_frmUtama.htm#frmUtama_SplitContainer1")
        Me.CNetHelpProvider.SetHelpNavigator(Me.SplitContainer1, System.Windows.Forms.HelpNavigator.Topic)
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.tvw)
        Me.CNetHelpProvider.SetHelpKeyword(Me.SplitContainer1.Panel1, "cdDVDmanager_Form_frmUtama.htm#frmUtama_Panel1")
        Me.CNetHelpProvider.SetHelpNavigator(Me.SplitContainer1.Panel1, System.Windows.Forms.HelpNavigator.Topic)
        Me.CNetHelpProvider.SetShowHelp(Me.SplitContainer1.Panel1, True)
        '
        'SplitContainer1.Panel2
        '
        Me.CNetHelpProvider.SetHelpKeyword(Me.SplitContainer1.Panel2, "cdDVDmanager_Form_frmUtama.htm#frmUtama_Panel2")
        Me.CNetHelpProvider.SetHelpNavigator(Me.SplitContainer1.Panel2, System.Windows.Forms.HelpNavigator.Topic)
        Me.CNetHelpProvider.SetShowHelp(Me.SplitContainer1.Panel2, True)
        Me.CNetHelpProvider.SetShowHelp(Me.SplitContainer1, True)
        Me.SplitContainer1.Size = New System.Drawing.Size(909, 645)
        Me.SplitContainer1.SplitterDistance = 172
        Me.SplitContainer1.TabIndex = 0
        '
        'Button1
        '
        Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CNetHelpProvider.SetHelpKeyword(Me.Button1, "cdDVDmanager_Form_frmUtama.htm#frmUtama_Button1")
        Me.CNetHelpProvider.SetHelpNavigator(Me.Button1, System.Windows.Forms.HelpNavigator.Topic)
        Me.Button1.Location = New System.Drawing.Point(3, 7)
        Me.Button1.Name = "Button1"
        Me.CNetHelpProvider.SetShowHelp(Me.Button1, True)
        Me.Button1.Size = New System.Drawing.Size(166, 23)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "Close Existing Form"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'tvw
        '
        Me.tvw.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tvw.BackColor = System.Drawing.Color.FromArgb(CType(CType(114, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(203, Byte), Integer))
        Me.tvw.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CNetHelpProvider.SetHelpKeyword(Me.tvw, "cdDVDmanager_Form_frmUtama.htm#frmUtama_tvw")
        Me.CNetHelpProvider.SetHelpNavigator(Me.tvw, System.Windows.Forms.HelpNavigator.Topic)
        Me.tvw.Location = New System.Drawing.Point(3, 36)
        Me.tvw.Name = "tvw"
        Me.CNetHelpProvider.SetShowHelp(Me.tvw, True)
        Me.tvw.Size = New System.Drawing.Size(166, 606)
        Me.tvw.TabIndex = 0
        '
        'CNetHelpProvider
        '
        Me.CNetHelpProvider.HelpNamespace = "cdDVDmanager.chm"
        '
        'frmUtama
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(95, Byte), Integer), CType(CType(143, Byte), Integer), CType(CType(175, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(909, 645)
        Me.Controls.Add(Me.SplitContainer1)
        Me.CNetHelpProvider.SetHelpKeyword(Me, "cdDVDmanager_Form_frmUtama.htm")
        Me.CNetHelpProvider.SetHelpNavigator(Me, System.Windows.Forms.HelpNavigator.Topic)
        Me.Name = "frmUtama"
        Me.CNetHelpProvider.SetShowHelp(Me, True)
        Me.Text = "Livan CD DVD Manager"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents tvw As System.Windows.Forms.TreeView
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents CNetHelpProvider As System.Windows.Forms.HelpProvider

End Class
