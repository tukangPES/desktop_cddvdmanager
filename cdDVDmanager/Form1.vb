﻿Option Strict On

Public Class frmUtama

    Private Sub frmUtama_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim aTreeNode As TreeNode
        aTreeNode = tvw.Nodes.Add("Menu")
        aTreeNode = aTreeNode.Nodes.Add("Querier")
        aTreeNode.Tag = "gui2.frmQuerier|gui2.dll"

    End Sub

    Private Sub tvw_NodeMouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeNodeMouseClickEventArgs) Handles tvw.NodeMouseDoubleClick
        ' launch
        Dim aForm As Form
        Dim aAssembly As System.Reflection.Assembly
        Dim aTn As TreeNode
        Dim aMyPath As String
        Dim aInfo() As String

        aTn = e.Node
        If aTn.Tag Is Nothing Then
        Else
            aInfo = Split(CStr(aTn.Tag), "|")

            aMyPath = Application.StartupPath
            aAssembly = System.Reflection.Assembly.LoadFile(aMyPath & "\" & aInfo(1))
            aForm = CType(aAssembly.CreateInstance(aInfo(0), True), Form)
            aForm.TopLevel = False
            aForm.Parent = SplitContainer1.Panel2
            aForm.Dock = DockStyle.Fill
            aForm.Show()
        End If
        
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        For Each aControl As Control In SplitContainer1.Panel2.Controls
            If TypeOf aControl Is Form Then
                CType(aControl, Form).Close()
            End If
        Next
    End Sub
End Class