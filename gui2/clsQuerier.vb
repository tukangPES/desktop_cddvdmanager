﻿Option Strict On

Imports System.Windows.Forms

Public Class clsQuerier
    Private _pathNamaFile As String
    Private _daftarDetail As New List(Of clsRowData)
    Public Event gotDetail(ByVal iDetail As clsRowData)

    Public Sub removeAllDetail()
        Dim aDetail As clsRowData
        For aInt As Integer = _daftarDetail.Count - 1 To 0 Step -1
            aDetail = _daftarDetail(aInt)
            _daftarDetail.Remove(aDetail)
            aDetail.Dispose()
        Next
    End Sub

    Public Sub read(ByVal iQuery As String)
        Dim aConnection As OleDb.OleDbConnection
        Dim aDataAdapter As New OleDb.OleDbDataAdapter
        aConnection = New OleDb.OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & _pathNamaFile)
        aDataAdapter.SelectCommand = aConnection.CreateCommand()
        If iQuery = "" Then
            aDataAdapter.SelectCommand.CommandText = "listAll"
            aDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure
        Else
            aDataAdapter.SelectCommand.CommandText = String.Format("select * from livan where namafile like '%{0}%' or folder like '%{0}%'", iQuery)
            aDataAdapter.SelectCommand.CommandType = CommandType.TableDirect
        End If

        Dim aDataset As DataSet
        aDataset = New DataSet
        aDataAdapter.Fill(aDataset)

        Dim aDetail As clsRowData
        Dim aDatarow As DataRow
        For aInt As Integer = 0 To aDataset.Tables(0).Rows.Count - 1
            aDetail = New clsRowData
            _daftarDetail.add(aDetail)
            aDatarow = aDataset.Tables(0).Rows(aInt)
            aDetail.folder = CStr(aDatarow("folder"))
            aDetail.namaFile = CStr(aDatarow("namaFile"))
            aDetail.ukuran = CInt(CStr(aDatarow("ukuran")))
            aDetail.volNameDrive = CStr(aDatarow("volumenamedrive"))

            RaiseEvent gotDetail(aDetail)
        Next

    End Sub

    Public Sub New(ByVal iNamaFile As String)
        _pathNamaFile = iNamaFile
    End Sub
End Class

Public Class clsRowData
    Implements IDisposable

    Public namaFile As String
    Public folder As String
    Public ukuran As Integer
    Public volNameDrive As String
    Public tagObject As Object
    Private _tag As DataGridViewRow

    Private disposedValue As Boolean = False        ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: free other state (managed objects).
            End If

            ' TODO: free your own state (unmanaged objects).
            ' TODO: set large fields to null.
        End If
        Me.disposedValue = True
    End Sub

    Public Property tagObjectJ() As DataGridViewRow
        Get
            Return _tag
        End Get
        Set(ByVal value As DataGridViewRow)
            _tag = value
        End Set
    End Property

#Region " IDisposable Support "

    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class