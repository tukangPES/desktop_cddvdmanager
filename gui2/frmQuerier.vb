﻿Option Strict On

Imports System.Windows.Forms
Imports guiControls
Imports System.Reflection
Imports System.Data.SqlClient

Public Class frmQuerier
    Private _colNamaFile As DataGridViewTextBoxColumn
    Private _colFolder As DataGridViewTextBoxColumn
    Private _colUkuran As DataGridViewTextBoxColumn
    Private _colVolNameDrive As DataGridViewTextBoxColumn

    Private WithEvents _kelas As gui2.clsQuerier

    Private _connection As OleDb.OleDbConnection
    Private _jumlahHalaman As Integer
    Private _dataAdapter As OleDb.OleDbDataAdapter
    Private _dataset As New DataSet
    Private _dataTablas As DataTable

    Private Sub frmQuerier_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        _connection.Close()
        _connection.Dispose()
    End Sub

    Private Sub frmQuerier_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'TESTDataSet.Livan' table. You can move, or remove it, as needed.
        'Me.LivanTableAdapter.Fill(Me.TESTDataSet.Livan)
        'siapkan dgv

        '_colNamaFile = clsDGVTextBoxColumn.createOne("0", "Nama File", 100)
        '_colFolder = clsDGVTextBoxColumn.createOne("1", "Folder Name", 300)
        '_colUkuran = clsDGVTextBoxColumn.createOne("2", "Ukuran", 100, enJenisCell.number0)
        '_colVolNameDrive = clsDGVTextBoxColumn.createOne("3", "Volume Name Drive", 200)

        'dgv.Columns.Add(_colNamaFile)
        'dgv.Columns.Add(_colFolder)
        'dgv.Columns.Add(_colUkuran)
        'dgv.Columns.Add(_colVolNameDrive)
        'dgv.AllowUserToAddRows = False


        '_kelas = New gui2.clsQuerier("test.mdb")
        If _connection.State = ConnectionState.Closed Then
            _connection.Open()
        End If
        lblStatus.Text = "Please wait while Livan CD DVD manager is loading for the first time"
        Application.DoEvents()
        startQuery()
        lblStatus.Text = ""
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        startQuery(txtQuery.Text)
    End Sub

    Private Sub startQuery(ByVal iNamaFile As String)
        Dim aTotalRecord As Integer
        Dim aQueryString As String
        Dim aTempo() As Integer
        If iNamaFile = "" Then
            aQueryString = "select count(0) from livan"
        Else
            aQueryString = "select count(0) from livan where namaFile like '%" & txtQuery.Text & "%'"
        End If

        Using bCommand As OleDb.OleDbCommand = _connection.CreateCommand
            bCommand.CommandText = aQueryString
            bCommand.CommandType = CommandType.Text
            aTotalRecord = CInt(bCommand.ExecuteScalar)
        End Using
        _jumlahHalaman = CInt(Math.Ceiling(aTotalRecord / 30))
        aTempo = (From i As Integer In Enumerable.Range(1, _jumlahHalaman) Select i).ToArray()
        lbl1.Text = "dari " & _jumlahHalaman & " halaman"

        If iNamaFile = "" Then
            aQueryString = "select * from livan"
        Else
            aQueryString = "select * from livan where namaFile like '%" & txtQuery.Text & "%'"
        End If

        Dim aCommand As OleDb.OleDbCommand = _connection.CreateCommand
        aCommand.CommandText = aQueryString
        aCommand.CommandType = CommandType.Text
        If _dataAdapter Is Nothing Then
            _dataAdapter = New OleDb.OleDbDataAdapter(aCommand)
        Else
            Try
                _dataAdapter.SelectCommand.Dispose()
            Catch ex As Exception
            End Try
            _dataAdapter.SelectCommand = aCommand
        End If

        _dataset.Clear()
        _dataAdapter.Fill(_dataset)
        _dataTablas = _dataset.Tables(0).Clone
        ComboBox1.DataSource = aTempo

    End Sub

    Private Sub startQuery()
        startQuery("")
    End Sub

    Private Sub _kelas_gotDetail(ByVal iDetail As clsRowData) Handles _kelas.gotDetail
        Dim aRow As DataGridViewRow
        Dim aProp As PropertyInfo
        Dim aProp2 As FieldInfo

        aProp = iDetail.GetType.GetProperty("tagobjectJ.name", BindingFlags.IgnoreCase Or BindingFlags.Public Or BindingFlags.Instance)
        aProp2 = iDetail.GetType.GetField("folder")

        aRow = dgv.Rows(dgv.Rows.Add())
        aRow.Cells(_colFolder.Index).Value = iDetail.folder
        aRow.Cells(_colNamaFile.Index).Value = iDetail.namaFile
        aRow.Cells(_colUkuran.Index).Value = iDetail.ukuran
        aRow.Cells(_colVolNameDrive.Index).Value = iDetail.volNameDrive

        aRow.Tag = iDetail
        iDetail.tagObject = aRow
    End Sub

    Public Sub New()
        InitializeComponent()
        Dim aConnString As String
        aConnString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=""TEST.MDB"";User Id=admin;Password=;"
        Try
            _connection = New OleDb.OleDbConnection(aConnString)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        Dim aAwal As Integer = (CInt(ComboBox1.Text) - 1) * 30
        _dataTablas.Clear()
        For aInt As Integer = aAwal To aAwal + 30 - 1
            Try
                _dataTablas.ImportRow(_dataset.Tables(0).Rows(aInt))
            Catch ex As Exception
                Exit For
            End Try
        Next
        ''langsung update
        'Dim aQueryString As String
        'aQueryString = "select * from livan"
        'If txtQuery.Text <> "" Then
        '    aQueryString = aQueryString & " where namaFile like '%" & txtQuery.Text & "%'"
        'End If
        'Try
        '    _dataset.Clear()
        '    _dataAdapter.Fill(_dataset)
        'Catch ex As Exception
        'End Try

        dgv.DataSource = _dataTablas
    End Sub
End Class